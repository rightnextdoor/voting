import stanford.karel.*;

public class VotingBox extends SuperKarel {
	
	public void run() {
		
		while(frontIsClear()) {
			move();
			if (beepersPresent()) {
			move();
			
			} else {
				cleanNorth();
				cleanSouth();
				move();
				}
		}
	}
	public void cleanNorth() {
		turnLeft();
		move();
		while (beepersPresent()) {
			pickBeeper();
		}
		turnAround();
		move();
		turnLeft();
	}
	public void cleanSouth() {
		turnRight();
		move();
		while (beepersPresent()) {
			pickBeeper();
		}
		turnAround();
		move();
		turnRight();
	}

}
